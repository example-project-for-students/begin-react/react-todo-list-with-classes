import React from 'react'

class NewItemInput extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      newTodoText: ''
    }
    this.inputEl = React.createRef()
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange (e) {
    this.setState({ newTodoText: e.target.value })
  }

  handleSubmit (e, text) {
    e.preventDefault()
    this.setState({ newTodoText: '' })
    this.inputEl.current.focus()
    this.props.addTodo(text)
  }

  componentDidMount () {
    this.inputEl.current.focus()
  }

  render () {
    return (
      <form onSubmit={(e) => this.handleSubmit(e, this.state.newTodoText)}>
        <label htmlFor="new-todo">Add new task: </label>
        <input
          id="new-todo"
          value={this.state.newTodoText}
          onChange={this.handleChange}
          ref={this.inputEl}
        />
        <button type="submit">Add</button>
      </form>
    )
  }
}

export default NewItemInput
