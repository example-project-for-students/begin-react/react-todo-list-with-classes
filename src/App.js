import React from 'react'
import './App.css';
import TodoList from './components/TodoList'
import TodoListFooter from './components/TodoListFooter'
import NewItemInput from './components/NewItemInput'


class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      items: [
        { id: 1, text: 'Buy bread', completed: false },
        { id: 2, text: 'Learn javascript', completed: false },
        { id: 3, text: 'Learn React', completed: true }
      ],
      showActive: false
    }
    this.handleCheck = this.handleCheck.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleFilter = this.handleFilter.bind(this)
    this.addTodo = this.addTodo.bind(this)
  }

  addTodo (newTodoText) {
    if (newTodoText.length === 0) {
      return
    }
    const newTodo = {
      id: Date.now(),
      text: newTodoText,
      completed: false
    }
    this.setState(state => ({
      items: state.items.concat(newTodo)
    }))
  }

  handleCheck (e, id) {
    const completed = e.target.checked
    const newItems = this.state.items.map(item => {
      if (item.id === id) {
        item.completed = completed
      }
      return item
    })
    this.setState({ items: newItems })
  }

  handleDelete (id) {
    this.setState(state => ({
      items: state.items.filter(item => item.id !== id)
    }))
  }

  handleFilter (active) {
    this.setState({ showActive: active })
  }

  render () {
    const filteredItems = this.state.showActive
    ? this.state.items.filter(item => !item.completed)
    : this.state.items

    return (
      <div className="App">
        <h1>Todo List - Classes</h1>
          <NewItemInput 
            addTodo={this.addTodo}
          />
          <TodoList 
            items={filteredItems}
            handleCheck={this.handleCheck}
            handleDelete={this.handleDelete}
          />
          <TodoListFooter 
            nrItemsLeft={this.state.items.filter(item => !item.completed).length}
            handleFilter={this.handleFilter}
          />
      </div>
    )
  }
}

export default App;
